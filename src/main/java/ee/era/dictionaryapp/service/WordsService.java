package ee.era.dictionaryapp.service;

import ee.era.dictionaryapp.model.OperationResult;
import ee.era.dictionaryapp.model.Words;
import ee.era.dictionaryapp.repository.WordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class WordsService {

    @Autowired
    private WordsRepository wordsRepository;

    public OperationResult deleteWords(int wordsNr) {
        try {
            org.springframework.util.Assert.isTrue(wordsNr > 0, "Word NR not specified!");
            org.springframework.util.Assert.isTrue(wordsRepository.wordsExists(wordsNr), "The specified word does not exist!");
            wordsRepository.deleteWords(wordsNr);
            return new OperationResult(true, "OK", wordsNr);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResult(false, message, null);
        }
    }

    public OperationResult updateWords(Words words) {
        try {
            org.springframework.util.Assert.isTrue(words != null, "Word data does not exist!");
            org.springframework.util.Assert.isTrue(words.getEesti() != null && !words.getEesti().equals(""), "Word eesti not specified!");
            org.springframework.util.Assert.isTrue(words.getInglise() != null && !words.getInglise().equals(""), "Word inglise not specified!");
            org.springframework.util.Assert.isTrue(words.getInglise2() != null && !words.getInglise2().equals(""), "Word inlgise2 not specified!");

            List<Words> allWordsByName = wordsRepository.getWords();
            wordsRepository.updateWords(words);
            return new OperationResult(true, "OK", words.getNr());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResult(false, message, null);
        }
    }

    public OperationResult addWords(Words words) {
        try {
            Assert.isTrue(words != null, "Words data does not exist!");
            Assert.isTrue(words.getEesti() != null && !words.getEesti().equals(""), "Words eesti not specified!");
            Assert.isTrue(words.getInglise() != null && !words.getInglise().equals(""), "Words inglise not specified!");
            Assert.isTrue(words.getInglise2() != null && !words.getInglise2().equals(""), "Words inglise2 not specified!");

            int wordsNr = wordsRepository.addWords(words);
            return new OperationResult(true, "OK", wordsNr);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResult(false, message, null);
        }
    }




}
