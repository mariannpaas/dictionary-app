package ee.era.dictionaryapp.repository;

import ee.era.dictionaryapp.model.Words;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class WordsRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Words> getWords() {
        return jdbcTemplate.query("select * from words order by eesti asc", mapWordsRows);
    }

    public Words getWords(int nr) {
        List<Words> words = jdbcTemplate.query("select * from words where nr = ?", new Object[]{nr}, mapWordsRows);
        return words.size() > 0 ? words.get(0) : null;
    }
    public boolean wordsExists(int wordsNr) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(nr) from words where nr = ?",
                new Object[]{wordsNr},
                Integer.class
        );
        return count != null && count > 0;
    }

    public void updateWords(Words words) {
        jdbcTemplate.update(
                "update words set eesti = ?, inglise = ?, inglise2 = ? where nr = ?",
                words.getEesti(), words.getInglise(), words.getInglise2(), words.getNr()
        );
    }
    public void deleteWords(int wordsNr) {
        jdbcTemplate.update("delete from words where nr = ?", wordsNr);
    }

    private RowMapper<Words> mapWordsRows = (rs, rowNum) -> {
        Words words = new Words();
        words.setNr(rs.getInt("nr"));
        words.setEesti(rs.getString("eesti"));
        words.setInglise(rs.getString("inglise"));
        words.setInglise2(rs.getString("inglise2"));
        return words;
    };

    public int addWords(Words words) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into words (`eesti`, `inglise`,  `inglise2`) values (?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, words.getEesti());
            ps.setString(2, words.getInglise());
            ps.setString(3, words.getInglise2());
            return ps;
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }

}
