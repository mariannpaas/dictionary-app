package ee.era.dictionaryapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DictionaryappApplication {

	public static void main(String[] args) {
		SpringApplication.run(DictionaryappApplication.class, args);
	}

}
