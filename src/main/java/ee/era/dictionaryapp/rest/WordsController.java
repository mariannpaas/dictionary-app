package ee.era.dictionaryapp.rest;

import ee.era.dictionaryapp.model.OperationResult;
import ee.era.dictionaryapp.model.Words;
import ee.era.dictionaryapp.repository.WordsRepository;
import ee.era.dictionaryapp.service.WordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/words")
@CrossOrigin("*")
public class WordsController {
    @Autowired
    private WordsRepository wordsRepository;

    @Autowired
    private WordsService wordsService;

    @GetMapping("/all")
    public List<Words> getWords() {
        return wordsRepository.getWords();
    }

    @GetMapping("/{wordsNr}")
    public Words getSingleWord(@PathVariable("wordsNr") int nr) {
        return wordsRepository.getWords(nr);
    }


    @DeleteMapping("/{wordsNr}")
    public ResponseEntity<OperationResult> deleteWords(@PathVariable("wordsNr") int nr) {
        OperationResult result = wordsService.deleteWords(nr);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<OperationResult> updateWords(@RequestBody Words words){
        OperationResult result = wordsService.updateWords(words);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/add")
    public ResponseEntity<OperationResult> addWords(@RequestBody Words words) {
        OperationResult result = wordsService.addWords(words);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

}
