package ee.era.dictionaryapp.model;

public class Words {
    private int nr;
    private String eesti;
    private String inglise;
    private String inglise2;

    public Words() {
    }

    public Words(int nr, String eesti, String inglise, String inglise2) {
        this.nr = nr;
        this.eesti = eesti;
        this.inglise = inglise;
        this.inglise2 = inglise2;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getEesti() {
        return eesti;
    }

    public void setEesti(String eesti) {
        this.eesti = eesti;
    }

    public String getInglise() {
        return inglise;
    }

    public void setInglise(String inglise) {
        this.inglise = inglise;
    }

    public String getInglise2() {
        return inglise2;
    }

    public void setInglise2(String inglise2) {
        this.inglise2 = inglise2;
    }
}
