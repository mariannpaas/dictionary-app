package ee.era.dictionaryapp.model;

public class OperationResult {
    private boolean isSuccess;
    private String message;
    private Integer wordsNr;

    public OperationResult(boolean isSuccess, String message, Integer wordsNr) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.wordsNr = wordsNr;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public Integer getWordsNr() {
        return wordsNr;
    }
}
